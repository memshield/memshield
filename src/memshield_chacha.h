/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _MEMSHIELD_PAGE_QUEUE_H
#define _MEMSHIELD_PAGE_QUEUE_H

#include <stdint.h>

#include "memshield_crypt.h"

#ifdef __cplusplus
extern "C" {
#endif

#define NUM_SLOTS 32	/* must be a power of 2 */
#define PAGE_DATA_SIZE 4096
#define PQ_CACHE_PADSIZE 64

struct page_data {
	char data[PAGE_DATA_SIZE];
};

struct pq_slots {
	struct page_data in[NUM_SLOTS];
};

struct page_params {
	uint32_t iv[CHACHA_IV_WORDS];
	uint32_t flag;
};

struct page_queue {
	union {
		uint32_t head;                /* owned by the consumer (dev) */
		char __pad1[PQ_CACHE_PADSIZE];
	};
	union {
		uint32_t tail_host;           /* owned by producer(s) (host) */
		char __pad2[PQ_CACHE_PADSIZE];
	};
	union {
		volatile uint32_t tail_dev;   /* tail device side */
		char __pad3[PQ_CACHE_PADSIZE];
	};
	uint32_t mask;
	struct pq_slots *dev_slots;
	struct page_params *params;
};

void memshield_start(struct page_queue *pq_array, int num_workers, void *key);
int memshield_get_numworkers(void);

#ifdef __cplusplus
}
#endif

#endif /* _MEMSHIELD_PAGE_QUEUE_H */
