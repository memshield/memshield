/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _USERFAULTFD_HELPER_H
#define _USERFAULTFD_HELPER_H

#include <stdlib.h>
#include <syscall.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "log.h"
#include "userfaultfd.h"

#define __NR_userfaultfd 323

static inline int __uffd_zero_mode(int uffd, void *addr, size_t size, int mode)
{
	struct uffdio_zeropage zp;

	zp.range.start = (unsigned long) addr;
	zp.range.len = size;
	zp.mode = mode;

	if (ioctl(uffd, UFFDIO_ZEROPAGE, &zp) == -1) {
		log_sys_info("ioctl UFFD zeropage failed (mode: %d)\n", mode);
		return -1;
	}

	log_info("uffd zeropage occurred (mode: %d)\n", mode);

	return 0;
}

#define uffd_zero(__uffd, __addr, __size)			\
		__uffd_zero_mode(__uffd, __addr, __size, 0)
#define uffd_zero_dontwake(__uffd, __addr, __size)		\
		__uffd_zero_mode(__uffd, __addr, __size,	\
				UFFDIO_ZEROPAGE_MODE_DONTWAKE)

static inline int __uffd_copy_mode(int uffd, void *src, void *dst, size_t size,
								int mode)
{
	struct uffdio_copy cp;

	cp.src = (unsigned long) src;
	cp.dst = (unsigned long) dst;
	cp.len = size;
	cp.mode = mode;

	if (ioctl(uffd, UFFDIO_COPY, &cp) == -1) {
		log_sys_info("ioctl UFFD copy failed (mode: %d)\n", mode);
		return -1;
	}

	log_info("uffd copy occurred (mode: %d)\n", mode);

	return 0;
}

#define uffd_copy_dontwake(__uffd, __src, __dst, __size)	\
		__uffd_copy_mode(__uffd, __src, __dst, __size,	\
				UFFDIO_COPY_MODE_DONTWAKE)
#define uffd_copy_wake(__uffd, __src, __dst, __size)		\
		__uffd_copy_mode(__uffd, __src, __dst, __size,	0)

static inline int uffd_wake(int uffd, void *addr, size_t size)
{
	struct uffdio_range range;

	range.start = (unsigned long) addr;
	range.len = size;

	if (ioctl(uffd, UFFDIO_WAKE, &range) == -1) {
		log_sys_info("ioctl UFFD wake failed\n");
		return -1;
	}

	log_info("uffd wakeup\n");

	return 0;
}

static inline int __uffd_register_mode(int uffd, void *addr,size_t size,
								int mode)
{
	struct uffdio_register uffdio_register;

	uffdio_register.range.start = (unsigned long) addr;
	uffdio_register.range.len = size;
	uffdio_register.mode = mode;

	if (ioctl(uffd, UFFDIO_REGISTER, &uffdio_register) == -1) {
		log_sys_info("uffd WP ioctl register failed\n");
		return -1;
	}

	log_info("address %p registred (mode: %d)\n", addr, mode);
	return 0;
}

#define uffd_register_missing(__uffd, __addr, __size)			\
			__uffd_register_mode(__uffd, __addr, __size, 	\
				UFFDIO_REGISTER_MODE_MISSING)

static inline int uffd_unregister(int uffd, void *addr, size_t size)
{
	struct uffdio_range range;

	range.start = (unsigned long) addr;
	range.len = size;

	/*
	 * UFFDIO_UNREGISTER returns EINVAL
	 * in case of invalid address (i.e shared memory)
	 *
	 * In libmemshield stealth mmunmap we don't recognize
	 * shared memory with respect to anonymous memory,
	 * so we are forced to unregister all addresses
	 *
	 */
	if (ioctl(uffd, UFFDIO_UNREGISTER, &range) == -1) {
		if (errno == EINVAL)
			goto exit_success;
		log_sys_info("ioctl unregister failed\n");
		return -1;
	}

exit_success:
	log_info("address %p unregistred\n", addr);

	return 0;
}

static int __attribute__((unused)) open_userfault_fd(void)
{
	struct uffdio_api uffdio_api;
	int fd;

	fd = syscall(__NR_userfaultfd, O_CLOEXEC | O_NONBLOCK);
	if (fd == -1) {
		log_sys_info("Userfaultfd syscall failed\n");
		return -1;
	}

	uffdio_api.api = UFFD_API;
	uffdio_api.features = UFFD_FEATURE_EVENT_FORK  |
					UFFD_FEATURE_EVENT_UNMAP;

        if (ioctl(fd, UFFDIO_API, &uffdio_api) == -1) {
                log_sys_info("ioctl-UFFDIO_API error\n");
		return -1;
	}

	log_info("UFFD ctx created (fd: %d) (features=0x%llx)\n", fd,
							uffdio_api.features);
	return fd;
}

static void close_userfault_fd(int fd)
{
	if (close(fd) == -1)
		log_warning("userfault fd close failed\n");
}

static inline int __uffd_mprotect_prot(void *addr, size_t size, int prot)
{
	if (mprotect(addr, size, prot) == -1) {
		log_sys_info("mprotect PROT_NONE failed\n");
		return -1;
	}

	log_info("Set mprotect\n");

	return 0;
}

#define uffd_mprotect_none(__addr, __size)		\
		__uffd_mprotect_prot(__addr, __size, PROT_NONE)
#define uffd_mprotect_rdonly(__addr, __size)		\
		__uffd_mprotect_prot(__addr, __size, PROT_READ)
#define uffd_mprotect_wronly(__addr, __size)		\
		__uffd_mprotect_prot(__addr, __size, PROT_WRITE)
#define uffd_mprotect_rdwr(__addr, __size)		\
		__uffd_mprotect_prot(__addr, __size, PROT_READ | PROT_WRITE)

static inline int __uffd_madvise(void *addr, size_t size, int behavior)
{
	log_info("Start madvise %p\n", addr);
	if (madvise(addr, size, behavior) == -1) {
		log_sys_info("madvise failed\n");
		return -1;
	}

	log_info("Set madvise %p\n", addr);

	return 0;
}
#define uffd_madvise_dontneed(__addr, __size)		\
				__uffd_madvise(__addr, __size, MADV_DONTNEED)

#endif  /* _USERFAULTFD_HELPER_H */
