/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _SLIDING_WINDOW_H
#define _SLIDING_WINDOW_H

#include <stdlib.h>

#include "linux_list.h"

struct sliding_window {
	unsigned int max_size, size;
	struct list_head window_head;
};

#define INIT_SLIDING_WINDOW(name, max_size)			\
	struct sliding_window name = {				\
		.window_head = LIST_HEAD_INIT(name.window_head);\
		.max_size = (max_size);				\
		.size = 0;					\
	}

struct sliding_window_node {
	unsigned long addr;
	struct list_head window;
};

static inline
struct sliding_window_node *alloc_sw_node(unsigned long addr)
{
	struct sliding_window_node *p = malloc(sizeof(*p));
	if (p == NULL)
		return NULL;

	p->addr = addr;
	return p;
}

static inline
void free_sw_node(struct sliding_window_node *p)
{
	free(p);
}

static inline
struct sliding_window_node *pop_sw_node(struct sliding_window *sw)
{
	struct list_head *tail;

	if (list_empty(&sw->window_head))
		return NULL;

	tail = sw->window_head.prev;
	list_del(tail);

	sw->size--;

	return list_entry(tail, struct sliding_window_node, window);
}

static inline
int push_sw_node(struct sliding_window *sw, struct sliding_window_node *node)
{
	if (sw->size > sw->max_size)
		return -1;

	list_add(&node->window, &sw->window_head);
	sw->size++;

	return 0;
}

static inline
struct sliding_window_node *search_sw_node(struct sliding_window *sw,
							unsigned long key)
{
	struct sliding_window_node *p;

	list_for_each_entry(p, &sw->window_head, window) {
		if (p->addr == key)
			return p;
	}

	return NULL;
}

static inline
struct sliding_window_node *remove_sw_node(struct sliding_window *sw,
							unsigned long key)
{
	struct sliding_window_node *p = search_sw_node(sw, key);

	if (p == NULL)
		return NULL;

	list_del(&p->window);
	sw->size--;

	return p;
}

#endif /* _SLIDING_WINDOW_H */
