/* SPDX-License-Identifier: GPL-2.0 */
#ifndef __CLOCK_H__
#define __CLOCK_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include "log.h"

static inline
uint64_t __clock_timespec_diff_us(struct timespec *start, struct timespec *stop)
{
	uint64_t sec, nsec;
	if ((stop->tv_nsec - start->tv_nsec) < 0) {
		sec = stop->tv_sec - start->tv_sec - 1;
		nsec = stop->tv_nsec - start->tv_nsec + 1000000000;
	} else {
		sec = stop->tv_sec - start->tv_sec;
		nsec = stop->tv_nsec - start->tv_nsec;
	}
	return sec * 1000000 + (nsec / 1000);
}
#define clock_abort_on_error(__cond) do {				\
	if (!(__cond))							\
		log_sys_error("clock_gettime failed\n");		\
	} while(0)

#define __cat(__name, __arg) __name ## __arg
#define clock_init(__name)						\
	struct timespec __cat(__name, start);				\
	struct timespec __cat(__name, stop)
#define clock_start(__name) 						\
	clock_abort_on_error(!clock_gettime(CLOCK_MONOTONIC,		\
						&(__cat(__name, start))))
#define clock_stop(__name)						\
	clock_abort_on_error(!clock_gettime(CLOCK_MONOTONIC,		\
						&(__cat(__name, stop))))
#define clock_get_delta_us(__name)					\
	__clock_timespec_diff_us(&(__cat(__name, start)),		\
						&(__cat(__name, stop)))
#define clock_print_delta_us(__name)					\
	log_profile("Clock %s - Elapsed time: %lu ms\n", #__name,	\
					clock_get_delta_us(__name))
#endif /*__CLOCK_H_*/
