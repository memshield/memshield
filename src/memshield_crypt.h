/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _MEMSHIELD_LIB_CRYPT_H
#define _MEMSHIELD_LIB_CRYPT_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define CHACHA_IV_WORDS		3
#define CHACHA_MASTER_KEY_WORDS	8

void memshield_init(void *key);
void memshield_encrypt_page(uint32_t tid, uint32_t *iv, void *page);
#define memshield_decrypt_page	memshield_encrypt_page

#ifdef __cplusplus
}
#endif

#endif /* _MEMSHIELD_LIB_CRYPT_H */
