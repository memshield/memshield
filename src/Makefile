MAKEDIR   := $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

CC	  := gcc
NVCC	  := nvcc
CFLAGS	  := -Wall -Wextra -O3 -march=native
ifneq ($(DEBUG), y)
CFLAGS    += -DNDEBUG
ifeq ($(VERBOSE), y)
CFLAGS    += -DVERBOSE
endif
else
CFLAGS    += -DVERBOSE
endif

MEMSHIELD    := memshield
MEMSHIELD_NC := memshield_nc		# without encryption
TARGETS   = $(MEMSHIELD)

ifeq ($(PROFILE), y)
CFLAGS    += -DPROFILE -DUSE_URANDOM_KEY
TARGETS   += $(MEMSHIELD_NC)
endif
NVCCFLAGS += --resource-usage -Xcompiler "$(CFLAGS)"

SOLIB	  := libmemshield.so
CFILES    := memshield.c memshield_rbtree.c memshield_chacha.cu memshield_crypt.c

all: $(SOLIB) $(TARGETS) tags

$(SOLIB): %.so : %.c
	$(CC) $(CFLAGS) -shared -fPIC $^ -o $@ -ldl -pthread

$(MEMSHIELD_NC): $(CFILES)
	$(NVCC) $(NVCCFLAGS) -DDISABLE_ENCRYPTION -o $@ $^

$(MEMSHIELD): $(CFILES)
	$(NVCC) $(NVCCFLAGS) -o $@ $^

tags:
	@ find . -type f -iname '*.[ch]' | xargs ctags -a
	@ find . -type f -iname '*.cu' | xargs ctags -a --langmap=C++:+.cu
clean:
	@ rm -vf $(SOLIB) $(MEMSHIELD) $(MEMSHIELD_NC) *.o *~ tags

.PHONY: all clean tags
