/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _MEMSHIELD_RANDOM_H
#define _MEMSHIELD_RANDOM_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "log.h"

#ifdef USE_GLIBC_GETRANDOM
#include <linux/random.h>
extern int getrandom(void *buf, size_t buflen, unsigned int flags);
#define memshield_getrandom getrandom
#else
#include <sys/syscall.h>
static __attribute__((unused))
int memshield_getrandom(void *buf, size_t buflen, unsigned int flags)
{
	const int ____NR_getrandom = 318;	/* x86_64 */
	return syscall(____NR_getrandom, buf, buflen, flags);
}
#endif

static inline void __memshield_random(void *buf, size_t len, int flags)
{
#ifndef NDEBUG
	int tmp;
	int *p = (int *) buf;
	const int debug_random_seed = 3617;
	(void) flags;
	srand(debug_random_seed);
	while (len > sizeof(int)) {
		*p++ = rand();
		len -= sizeof(int);
	}

	if (len > 0) {
		tmp = rand();
		memcpy(p, &tmp, len);
	}
#else
	int rd = 0;
	while (len) {
		rd = memshield_getrandom(buf + rd, len, flags);
		if (rd == -1) {
			perror("getrandom failed!\n");
			exit(EXIT_FAILURE);
		}
		len -= rd;
	}
#endif
}

#ifndef GRND_RANDOM
#define GRND_RANDOM 0x0002
#endif
#if USE_URANDOM_KEY
#define memshield_random memshield_urandom
#else
#define memshield_random(_buf, _len) __memshield_random(_buf, _len, GRND_RANDOM)
#endif
#define memshield_urandom(_buf, _len) __memshield_random(_buf, _len, 0)

#endif /* _MEMSHIELD_RANDOM_H */
