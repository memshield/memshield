/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _LOG_H
#define _LOG_H

#include <stdio.h>
#include <unistd.h>

#define __log_stderr(__format, ...)\
                        fprintf(stderr, __format, ##__VA_ARGS__)
#define __log_stdout(__format, ...)\
                        fprintf(stdout, __format, ##__VA_ARGS__)
#define log_error(__format, ...)		do {		\
                        __log_stderr("EE Error: " __format, ##__VA_ARGS__); \
		} while(0)
#define log_sys_error(__format, ...)		do {		\
			perror("EE Error: errno");		\
                        log_error(__format, ##__VA_ARGS__);	\
		} while(0)
#define log_fatal_error(__format, ...)       do {		\
                        log_error(__format, ##__VA_ARGS__);	\
                        _exit(EXIT_FAILURE);			\
                } while(0)
#define log_warning(__format, ...)				\
                        __log_stderr("WW Warning: " __format, ##__VA_ARGS__)
#define log_warn log_warning
#define log_notice(__format, ...)					\
                        __log_stderr("NN Notice: " __format, ##__VA_ARGS__)
#ifdef VERBOSE
#define log_info(__format, ...)					\
                        __log_stderr("II Info: " __format, ##__VA_ARGS__)
#else
#define log_info(__format, ...)		do {} while(0)
#endif

#ifdef VERBOSE
#define log_sys_info(__format, ...)		do {		\
			perror("II Info: errno");		\
                        log_info(__format, ##__VA_ARGS__);	\
		} while(0)
#else
#define log_sys_info(__format, ...)		do {} while(0)
#endif

#ifndef NDEBUG
#define log_debug(__format, ...)					\
                        __log_stderr("DD Debug: " __format, ##__VA_ARGS__)
#else
#define log_debug(__format, ...)	do {} while(0)
#endif

#ifdef PROFILE
#define log_profile(__format, ...)					\
                        __log_stderr("PP Profile: " __format, ##__VA_ARGS__)
#else
#define log_profile(__format, ...)	do {} while(0)
#endif


#endif /* _LOG_H */
