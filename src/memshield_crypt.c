// SPDX-License-Identifier: GPL-2.0
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <sched.h>
#include <cuda.h>
#include <cuda_runtime.h>

#include "log.h"
#include "memshield_chacha.h"
#include "memshield_crypt.h"

#define PAGE_SIZE sysconf(_SC_PAGESIZE)

#define try_cuda(func, ...) do {				\
	cudaError_t err = func(__VA_ARGS__);			\
	if (err != cudaSuccess) {				\
		log_error("%s\n", cudaGetErrorString(err));	\
		exit(EXIT_FAILURE);				\
	}							\
} while (0)

static int num_workers;
static struct page_queue *pq_array;

static void pq_init(struct page_queue *q)
{
	q->tail_dev = q->tail_host = q->head = 0u;
	q->mask = (NUM_SLOTS - 1);
	if (PAGE_DATA_SIZE != PAGE_SIZE) {
		log_error("Invalid page data size\n");
		exit(EXIT_FAILURE);
	}

	try_cuda(cudaMallocHost, (void **)&q->dev_slots,
		 sizeof(struct pq_slots));
	try_cuda(cudaHostAlloc, (void **)&q->params,
		 NUM_SLOTS * sizeof(q->params[0]), cudaHostAllocDefault);
	memset(q->params, 0, NUM_SLOTS * sizeof(q->params[0]));
}

static uint32_t pq_submit_async(struct page_queue *q, const void *pg,
				uint32_t * iv)
{
	uint32_t handle, idx;
	struct pq_slots *ps = q->dev_slots;
	const uint32_t mask = q->mask;

	/* take a slot */
	handle = __atomic_fetch_add(&q->tail_host, 1u, __ATOMIC_ACQUIRE);

	while (handle - q->head >= mask)
		sched_yield();

	idx = handle & mask;

	memcpy(ps->in[idx].data, pg, PAGE_SIZE);

	memcpy(&q->params[idx], iv, CHACHA_IV_WORDS * sizeof(iv[0]));
	__atomic_store_n(&q->params[idx].flag, 1, __ATOMIC_RELEASE);

	idx = __atomic_load_n(&q->tail_dev, __ATOMIC_RELAXED);
	while (handle >= idx) {
		if (__atomic_compare_exchange_n(&q->tail_dev, &idx, handle + 1,
						1, __ATOMIC_RELEASE,
						__ATOMIC_RELAXED))
			break;
	}

	return handle;
}

static inline void pq_get_result(struct page_queue *q, uint32_t handle,
				 void *res)
{
	uint32_t idx = handle & q->mask;

	while (__atomic_load_n(&q->params[idx].flag, __ATOMIC_ACQUIRE))
		sched_yield();

	memcpy(res, q->dev_slots->in[idx].data, PAGE_DATA_SIZE);
}

void memshield_encrypt_page(uint32_t tid, uint32_t * iv, void *out)
{
	uint32_t handle;
	struct page_queue *pq;

	pq = &pq_array[tid % num_workers];
	handle = pq_submit_async(pq, out, iv);
	pq_get_result(pq, handle, out);
}

void memshield_init(void *key)
{
	int i;
	void *key_page;

	try_cuda(cudaSetDeviceFlags, cudaDeviceScheduleBlockingSync);
	num_workers = memshield_get_numworkers();

	try_cuda(cudaHostAlloc, &key_page, PAGE_SIZE, cudaHostAllocDefault);
	memcpy(key_page, key, CHACHA_MASTER_KEY_WORDS * sizeof(uint32_t));

	try_cuda(cudaHostAlloc, (void **)&pq_array,
		 num_workers * sizeof(struct page_queue), cudaHostAllocDefault);

	for (i = 0; i < num_workers; ++i)
		pq_init(pq_array + i);

	log_info("memshield_init(): num_workers=%d, key_page=%p, pq_array=%p\n",
	         num_workers, (void *)key_page, (void *)pq_array);

	memshield_start(pq_array, num_workers, key_page);
}
