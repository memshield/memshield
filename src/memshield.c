// SPDX-License-Identifier: GPL-2.0
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>
#include <pthread.h>
#include <poll.h>
#include <alloca.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <signal.h>
#include <sys/prctl.h>
#include <libgen.h>

#ifndef MFD_CLOEXEC
#define NO_MEMFD_WRAPPER
#include <sys/syscall.h>
#endif

#include "log.h"
#include "memshield_rbtree.h"
#include "sliding_window.h"
#include "userfaultfd_helper.h"
#ifndef DISABLE_ENCRYPTION
#include "memshield_crypt.h"
#endif
#include "memshield_random.h"
#ifdef PROFILE
#include "clock.h"
#endif

#define LISTENQ	1024
#define socket_mask (S_IRUSR|S_IWUSR|S_IXUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)

unsigned int sliding_window_size = 5;

static long PAGE_SIZE;
static void *zero_page;

struct memshield_context {
	int sd;
	int uffd;
	pid_t pid;
	void *shared_page;
	struct rb_root tree_root;
	struct sliding_window plain_window;
	uint32_t ticket;
#ifdef PROFILE
	uint64_t pagefault_number;
	uint64_t pagefault_time_us;
	uint64_t unmap_number;
	uint64_t unmap_time_us;
#ifndef DISABLE_ENCRYPTION
	uint64_t encryption_number;
	uint64_t encryption_time_us;
	uint64_t decryption_number;
	uint64_t decryption_time_us;
#endif
#endif
};

#ifndef DISABLE_ENCRYPTION
static void memshield_server_get_iv(struct memshield_context *ctx,
				    uint64_t client_addr, uint32_t * page_iv)
{
	page_iv[0] = (uint32_t) ctx->pid;
	page_iv[1] = (uint32_t) client_addr;
	page_iv[2] = (uint32_t) (client_addr >> 32);
}

static
void memshield_server_decrypt_page(struct memshield_context *ctx,
				   uint64_t client_addr, void *addr)
{
	uint32_t page_iv[CHACHA_IV_WORDS];
#ifdef PROFILE
	clock_init(decryption_clock);
	clock_start(decryption_clock);
#endif
	memshield_server_get_iv(ctx, client_addr, page_iv);
	memshield_decrypt_page(ctx->ticket, page_iv, addr);
#ifdef PROFILE
	clock_stop(decryption_clock);
	ctx->decryption_number++;
	ctx->decryption_time_us += clock_get_delta_us(decryption_clock);
#endif
}

static
void memshield_server_encrypt_page(struct memshield_context *ctx,
				   uint64_t client_addr, void *addr)
{
	uint32_t page_iv[CHACHA_IV_WORDS];
#ifdef PROFILE
	clock_init(encryption_clock);
	clock_start(encryption_clock);
#endif
	memshield_server_get_iv(ctx, client_addr, page_iv);
	memshield_encrypt_page(ctx->ticket, page_iv, addr);
#ifdef PROFILE
	clock_stop(encryption_clock);
	ctx->encryption_number++;
	ctx->encryption_time_us += clock_get_delta_us(encryption_clock);
#endif
}
#else /* DISABLE_ENCRYPTION */
#define memshield_server_decrypt_page(__ctx, __client_addr, __addr) \
								do {} while(0)
#define memshield_server_encrypt_page(__ctx, __client_addr, __addr) \
								do {} while(0)
#endif

static
int __recv_page_from_libmemshield(struct memshield_context *ctx,
				  unsigned long addr, void *page)
{
	char c;
	log_debug("sending addr 0x%lx to libmemshield\n", addr);
	if (send(ctx->sd, &addr, sizeof(addr), 0) != sizeof(addr)) {
		log_sys_error("send() failed!\n");
		return -1;
	}
	if (recv(ctx->sd, &c, sizeof(char), 0) != sizeof(char)) {
		log_sys_error("recv() failed\n");
		return -1;
	}
	memcpy(page, ctx->shared_page, PAGE_SIZE);
	memset(ctx->shared_page, 0, PAGE_SIZE);
	log_debug("receiving addr 0x%lx to libmemshield\n", addr);

	return 0;
}

static
int __handle_pagefault(struct memshield_context *ctx, struct uffd_msg *msg)
{
	void *client_addr, *wake_addr;
	struct memshield_node *node;
	struct sliding_window_node *sw_node;

	client_addr = (void *)msg->arg.pagefault.address;
	node = rb_memshield_search(&ctx->tree_root, client_addr);
	log_debug("PAGEFAULT event on addr: %p\n", client_addr);

	wake_addr = client_addr;
	if (node == NULL) {
		node = rb_alloc_memshield_node(client_addr, PAGE_SIZE);
		if (node == NULL) {
			log_error("rb_alloc_memshield_node() failed!\n");
			return -1;
		}

		if (rb_memshield_insert(&ctx->tree_root, node)) {
			rb_free_memshield_node(node, PAGE_SIZE);
			log_error("rb_memshield_insert() failed!\n");
			return -1;
		}

		if (uffd_zero_dontwake(ctx->uffd, client_addr, PAGE_SIZE)) {
			rb_memshield_remove(&node->node, &ctx->tree_root);
			rb_free_memshield_node(node, PAGE_SIZE);
			log_error("uffd_zero() failed!\n");
			return -1;
		}
		node->page_plain = 1;
	} else {
		if (!node->page_plain) {
			memshield_server_decrypt_page(ctx,
						      (uint64_t) client_addr,
						      node->local_addr);
			if (uffd_copy_dontwake(ctx->uffd, node->local_addr,
						      client_addr, PAGE_SIZE)) {
				rb_memshield_remove(&node->node,
						    &ctx->tree_root);
				rb_free_memshield_node(node, PAGE_SIZE);
				log_error("uffd_remap() failed!\n");
				return -1;
			}
		} else {
			if (uffd_zero(ctx->uffd, client_addr, PAGE_SIZE)) {
				rb_memshield_remove(&node->node,
						    &ctx->tree_root);
				rb_free_memshield_node(node, PAGE_SIZE);
				log_error("uffd_zero plain failed\n");
				return -1;
			}
			return 0;
		}

		node->page_plain = 1;
		memset(node->local_addr, 0, PAGE_SIZE);
	}

	sw_node = alloc_sw_node((unsigned long)client_addr);
	if (sw_node == NULL) {
		log_error("alloc_sw_node() failed!\n");
		return -1;
	}

	if (push_sw_node(&ctx->plain_window, sw_node)) {
		free_sw_node(sw_node);
		log_error("free_sw_node() failed!\n");
		return -1;
	}

	if (ctx->plain_window.size <= ctx->plain_window.max_size)
		goto wake;

	sw_node = pop_sw_node(&ctx->plain_window);
	if (sw_node == NULL) {	/* should not happen */
		log_error("pop_sw_node() failed!\n");
		return -1;
	}

	client_addr = (void *)sw_node->addr;
	free_sw_node(sw_node);

	node = rb_memshield_search(&ctx->tree_root, client_addr);
	if (node == NULL) {
		log_error("no node found from sliding_window\n");
		return -1;
	}

	if (__recv_page_from_libmemshield(ctx,
					  (unsigned long)client_addr,
					  node->local_addr)) {
		log_error("__recv_page_from_libmemshield() failed!\n");
		return -1;
	}

	memshield_server_encrypt_page(ctx, (uint64_t) client_addr,
				      node->local_addr);
	node->page_plain = 0;
wake:
	if (uffd_wake(ctx->uffd, wake_addr, PAGE_SIZE)) {
		log_error("uffd_wake() failed!\n");
		return -1;
	}

	return 0;
}

static
int __handle_unmap(struct memshield_context *ctx, struct uffd_msg *msg)
{
	void *start_addr;
	void *end_addr;
	struct memshield_node *node;
	struct sliding_window_node *sw_node;

	start_addr = (void *)msg->arg.remove.start;
	end_addr = (void *)msg->arg.remove.end;

	log_info("munmap: start_addr: %p - length: %lu\n", start_addr,
		 (end_addr - start_addr) / PAGE_SIZE);

	if ((end_addr - start_addr) % PAGE_SIZE) {
		log_error("unexpected munmap range!\n");
		return -1;
	}

	for (; start_addr < end_addr; start_addr += PAGE_SIZE) {
		sw_node = remove_sw_node(&ctx->plain_window,
					 (unsigned long)start_addr);
		free_sw_node(sw_node);

		node = rb_memshield_search(&ctx->tree_root, start_addr);
		if (node) {
			rb_memshield_remove(&node->node, &ctx->tree_root);
			rb_free_memshield_node(node, PAGE_SIZE);
		}
	}

	return 0;
}

static
int __handle_userfaultfd(struct memshield_context *ctx, struct uffd_msg *msg)
{
	int rc = 0;
	uint8_t event = msg->event;
#ifdef PROFILE
	clock_init(event_clock);
	clock_start(event_clock);
#endif
	switch (event) {

	case UFFD_EVENT_PAGEFAULT:
		if (msg->arg.pagefault.flags & UFFD_PAGEFAULT_FLAG_WP)
			log_error("PAGEFAULT WP event on addr: 0x%llx\n",
				  msg->arg.pagefault.address);
		else
			rc = __handle_pagefault(ctx, msg);
#ifdef PROFILE
		clock_stop(event_clock);
		ctx->pagefault_number++;
		ctx->pagefault_time_us += clock_get_delta_us(event_clock);
#endif
		break;
	case UFFD_EVENT_UNMAP:
		log_debug("UNMAP event\n");
		rc = __handle_unmap(ctx, msg);
#ifdef PROFILE
		clock_stop(event_clock);
		ctx->unmap_number++;
		ctx->unmap_time_us += clock_get_delta_us(event_clock);
#endif
		break;
	case UFFD_EVENT_FORK:
		log_error("unsupported FORK event\n");
		break;
	case UFFD_EVENT_REMAP:
		log_error("unsupported REMAP event\n");
		break;
	case UFFD_EVENT_REMOVE:
		log_error("unsupported REMOVE event\n");
		break;
	default:
		log_error("unknown event (%#x)\n", event);
		return -1;
	}

	return rc;
}

static
int __handle_userfaultfd_read(int fd, struct uffd_msg *msg)
{
	int ret;

redo:
	ret = read(fd, msg, sizeof(struct uffd_msg));
	if (ret == 0) {
		log_debug("EOF on userfaultfd\n");
		return 0;
	}
	if (ret == -1) {
		if (errno == EAGAIN)
			return 0;
		if (errno == EINTR || errno == EWOULDBLOCK)
			goto redo;
		log_sys_error("read userfaultfd failed (%d)\n", ret);
		return -1;
	}
	if (ret != sizeof(struct uffd_msg)) {
		log_error("Unespected size on UFFD read\n");
		return -1;
	}
	return 1;
}

static
int process_is_dead(pid_t pid)
{
	if (kill(pid, 0) == -1) {
		if (errno == ESRCH) {
			log_debug("Process %d is dead\n", pid);
			return 1;
		} else {
			log_sys_error("Kill (pid: %d) failed\n", pid);
			return -1;
		}
	}
	return 0;
}

#ifdef PROFILE
#define __memshield_profile_get_avg(__ctx, __avg, event)	do {		\
		(*__avg) = ((__ctx)->__cat(event, _number)) ?		\
			((double)(__ctx)->__cat(event, _time_us) /	\
			((double)(__ctx)->__cat(event, _number))) : 0.0;\
	} while(0)
#define __memshield_profile_print(__ctx, __avg, event)	do {		\
		log_profile(#event " - %lu - %lu - %lf\n",		\
				((__ctx)->__cat(event, _number)),	\
				((__ctx)->__cat(event, _time_us)),	\
				(__avg));				\
	} while(0)

static
void memshield_print_profile_info(struct memshield_context *ctx)
{
	double avg;

	log_profile("event name - number - tot time (us) - avg time (us)\n");
	__memshield_profile_get_avg(ctx, &avg, pagefault);
	__memshield_profile_print(ctx, avg, pagefault);
	__memshield_profile_get_avg(ctx, &avg, unmap);
	__memshield_profile_print(ctx, avg, unmap);
#ifndef DISABLE_ENCRYPTION
	__memshield_profile_get_avg(ctx, &avg, encryption);
	__memshield_profile_print(ctx, avg, encryption);
	__memshield_profile_get_avg(ctx, &avg, decryption);
	__memshield_profile_print(ctx, avg, decryption);
#endif
}
#else
#define memshield_print_profile_info(__ctx) do {} while(0)
#endif /* PROFILE */

static
void handle_userfaultfd(struct memshield_context *ctx)
{
	int ret;
	struct uffd_msg msg;
	struct pollfd pollfd;
	const int timeout_ms = 1000;

	int fd = ctx->uffd;

	for (;;) {
		pollfd.fd = fd;
		pollfd.events = POLLIN | POLLERR;

		ret = poll(&pollfd, 1, timeout_ms);
		if (ret == -1) {
			log_sys_error("poll failed\n");
			return;
		}
		if ((ret == 0) && process_is_dead(ctx->pid)) {
			memshield_print_profile_info(ctx);
			break;
		}

		if (pollfd.revents & POLLERR) {
			log_warning("poll error\n");
			continue;
		}

		log_debug("poll returns: %d; "
			  "POLLIN: %d, POLLERR: %d\n", ret,
			  (pollfd.revents & POLLIN) != 0,
			  (pollfd.revents & POLLERR) != 0);

		ret = __handle_userfaultfd_read(fd, &msg);
		if (ret < 0) {
			log_error("__handle_userfaultfd_read failed\n");
			return;
		}
		if (!ret) {
			log_debug("__handle_userfaultfd_read returns 0\n");
			continue;
		}
		ret = __handle_userfaultfd(ctx, &msg);
		if (ret < 0) {
			log_error("__handle_userfaultfd() failed\n");
			return;
		}
		if (ret == 1)
			return;
	}
}

static int recvfd_from_socket(int sd)
{
	char buff;
	int recvfd;
	struct msghdr msg;
	struct iovec iov[1];

	union {
		struct cmsghdr cm;
		char control[CMSG_SPACE(sizeof(int))];
	} control_un;
	struct cmsghdr *cmsg;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof control_un.control;

	msg.msg_name = NULL;
	msg.msg_namelen = 0;

	iov[0].iov_base = &buff;
	iov[0].iov_len = sizeof buff;
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;

	if (recvmsg(sd, &msg, 0) == -1) {
		log_sys_error("error on recvmsg()");
		return -1;
	}

	cmsg = CMSG_FIRSTHDR(&msg);
	if (cmsg == NULL) {
		log_sys_error("error on handle_fd() #0\n");
		return -1;
	}

	if (cmsg->cmsg_len != CMSG_LEN(sizeof(int))) {
		log_sys_error("error on handle_fd() #1\n");
		return -1;
	}

	if (cmsg->cmsg_level != SOL_SOCKET) {
		log_sys_error("error on handle_fd() #2\n");
		return -1;
	}

	if (cmsg->cmsg_type != SCM_RIGHTS) {
		log_sys_error("error on handle_fd() #3\n");
		return -1;
	}

	memcpy(&recvfd, CMSG_DATA(cmsg), sizeof(int));

	return recvfd;
}

struct thread_data {
	long cd;
	uint32_t ticket;
};

static
int memshield_memfd_create(const char *name, unsigned int flags)
{
#ifdef NO_MEMFD_WRAPPER

#ifndef F_LINUX_SPECIFIC_BASE
#define F_LINUX_SPECIFIC_BASE 1024
#endif

#ifndef F_ADD_SEALS
#define F_ADD_SEALS (F_LINUX_SPECIFIC_BASE + 9)
#define F_GET_SEALS (F_LINUX_SPECIFIC_BASE + 10)

#define F_SEAL_SEAL     0x0001	/* prevent further seals from being set */
#define F_SEAL_SHRINK   0x0002	/* prevent file from shrinking */
#define F_SEAL_GROW     0x0004	/* prevent file from growing */
#define F_SEAL_WRITE    0x0008	/* prevent writes */
#endif
#define MFD_CLOEXEC		0x0001U
#define MFD_ALLOW_SEALING	0x0002U
#define MFD_HUGETLB		0x0004U
	return syscall(__NR_memfd_create, name, flags);
#else
	return memfd_create(name, flags);
#endif
}

static int memfd_add_seals(int memfd)
{
	if (fcntl(memfd, F_ADD_SEALS, F_SEAL_SHRINK) == -1) {
		log_sys_error("memfd fcntl failed (shrink)\n");
		return -1;
	}
	if (fcntl(memfd, F_ADD_SEALS, F_SEAL_GROW) == -1) {
		log_sys_error("memfd fcntl failed (grow)\n");
		return -1;
	}
	if (fcntl(memfd, F_ADD_SEALS, F_SEAL_SEAL) == -1) {
		log_sys_error("memfd fcntl failed (seal)\n");
		return -1;
	}
	return 0;
}

static int open_memfd(int pid)
{
	const size_t memfd_name_max_length = 64;
	char memfd_name[memfd_name_max_length];
	int memfd;

	snprintf(memfd_name, memfd_name_max_length, "memshield-%d", pid);
	memfd = memshield_memfd_create(memfd_name, MFD_ALLOW_SEALING);
	if (memfd == -1) {
		log_sys_error("memfd failed");
		return -1;
	}
	if (ftruncate(memfd, PAGE_SIZE) == -1) {
		log_sys_error("memfd ftruncate failed");
		goto err_ftruncate;
	}
	return memfd;
err_ftruncate:
	close(memfd);
	return -1;
}

static int sendfd_on_socket(int sd, int fd)
{
	char buff;
	struct msghdr msg;
	struct iovec iov[1];
	union {
		struct cmsghdr cm;
		char control[CMSG_SPACE(sizeof(int))];
	} control_un;
	struct cmsghdr *cmptr;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof(control_un.control);
	cmptr = CMSG_FIRSTHDR(&msg);
	cmptr->cmsg_len = CMSG_LEN(sizeof(int));
	cmptr->cmsg_level = SOL_SOCKET;
	cmptr->cmsg_type = SCM_RIGHTS;
	memcpy(CMSG_DATA(cmptr), &fd, sizeof(int));
	msg.msg_name = NULL;
	msg.msg_namelen = 0;
	iov[0].iov_base = &buff;
	iov[0].iov_len = 1;
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;

	if ((sendmsg(sd, &msg, 0)) == -1) {
		log_sys_error("error on send fd sendmsg()\n");
		return -1;
	}

	return 0;
}

static void *handle_fd(void *arg)
{
	int i, ret, sd, uffd, memfd;
	struct memshield_context ctx;
	struct thread_data *td;
	struct ucred creds;
	socklen_t creds_size;
	uint32_t ticket;

	creds_size = sizeof(creds);
	td = arg;
	sd = td->cd;
	ticket = td->ticket;
	free(td);

	uffd = recvfd_from_socket(sd);
	if (uffd == -1) {
		log_error("recv_uufd()\n");
		goto uffd_err;
	}

	if (getsockopt(sd, SOL_SOCKET, SO_PEERCRED, &creds, &creds_size) < 0) {
		log_sys_error("getsockopt failed\n");
		goto recv_err;
	}
	log_notice("Starting thread (pid: %d)\n", creds.pid);

	memset(&ctx, 0, sizeof(ctx));
	ctx.sd = sd;
	ctx.ticket = ticket;
	ctx.uffd = uffd;
	ctx.pid = creds.pid;
	ctx.tree_root = RB_ROOT;
	ctx.plain_window.max_size = sliding_window_size;
	INIT_LIST_HEAD(&ctx.plain_window.window_head);
	memfd = open_memfd(ctx.pid);
	if (memfd == -1) {
		log_error("memfd failed\n");
		goto memfd_err;
	}
	ctx.shared_page = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE,
			       MAP_SHARED | MAP_POPULATE, memfd, 0);
	if (ctx.shared_page == MAP_FAILED) {
		log_sys_error("shared page mmap failed\n");
		goto err_mmap;
	}
	if (memfd_add_seals(memfd) != 0) {
		log_sys_error("memfd seals failed\n");
		goto err_seals;
	}
	if (sendfd_on_socket(sd, memfd) != 0) {
		log_error("unable to send memfd\n");
		goto err_sendfd;
	}
	ret = recv(sd, &i, sizeof(char), 0);
	if (ret != (long)sizeof(char)) {
		log_error("unable to recv memfd msg\n");
		goto err_memfd_recv;
	}
	if (close(memfd) == -1) {
		log_error("unable to close memfd\n");
		memfd = -1;
		goto err_close_memfd;
	}
	memfd = -1;
	handle_userfaultfd(&ctx);
err_close_memfd:
err_memfd_recv:
err_sendfd:
err_seals:
	munmap(ctx.shared_page, PAGE_SIZE);
err_mmap:
	if (memfd != -1)
		close(memfd);
memfd_err:
recv_err:
	close_userfault_fd(uffd);
uffd_err:
	close(sd);
	log_notice("Ending thread (pid: %d)\n", creds.pid);
	return NULL;
}

static char *which_program(const char *program)
{
	FILE *pipe;
	char *buffer, *cmd;
	const size_t buflen = 1024;

	if (program == NULL)
		return NULL;
	buffer = malloc(buflen);
	if (buffer == NULL)
		return NULL;
	cmd = alloca(strlen(program) + 64);
	sprintf(cmd, "which %s 2> /dev/null", program);
	pipe = popen(cmd, "r");
	if (pipe == NULL)
		goto err_popen;
	if (fgets(buffer, buflen, pipe) == NULL)
		goto err;
	if (strlen(buffer) == 0)
		goto err;
	buffer[strlen(buffer) - 1] = '\0';
	pclose(pipe);
	return buffer;
err:
	pclose(pipe);
err_popen:
	free(buffer);
	return NULL;
}

#define which_nvidia_modprobe()	which_program("nvidia-modprobe");

static int get_file_stats(const char *pathname, mode_t *mode, uid_t *uid)
{
	struct stat st;

	if (pathname == NULL)
		log_fatal_error("Pathname null in get_file_stats\n");
	if (stat(pathname, &st))
		return -1;
	if (mode != NULL)
		*mode = st.st_mode;
	if (uid != NULL)
		*uid = st.st_uid;
	return 0;
}

#define get_file_stat_mode(__pathname, __mode)				\
	get_file_stats(__pathname, __mode, NULL)

#define get_file_stat_uid(__pathname, __uid)				\
	get_file_stats(__pathname, NULL, __uid)

#define file_exists(__pathname)						\
	(!(get_file_stats(__pathname, NULL, NULL)))

#define bitmask_is_set(__pathname, __bitmask)	({			\
	mode_t __m;							\
	if (get_file_stat_mode(__pathname, &__m))			\
		log_fatal_error("Invalid pathname %s\n", __pathname);\
	!!((__bitmask) & __m);	})

#define suid_bit_is_set(__pathname) bitmask_is_set(__pathname, S_ISUID)

#define file_is_mine(__pathname)	({				\
	uid_t __uid;							\
	if (get_file_stat_uid(__pathname, &__uid))			\
		log_fatal_error("Invalid pathname %s\n", __pathname);\
	((__uid) == getuid());	})

static void check_nvidia_modprobe(void)
{
	char *str;

	str = which_nvidia_modprobe();
	if (str == NULL) {
		log_warning("Unable to find NVIDIA modprobe\n");
		return;
	}
	if (suid_bit_is_set(str)) {
		log_error("*********************************\n");
		log_error("WARNING!!!\n");
		log_error("nvidia-modprobe suid is set!\n");
		log_error("This could be a security risk!\n");
		log_error("*********************************\n\n");
	}
	free(str);
}

static void check_right_permission(const char *path)
{
	if (!file_exists(path))
		return;
	if (!file_is_mine(path)) {
		log_error("*********************************\n");
		log_error("WARNING!!!\n");
		log_error("'%s' file owner is not this user!\n", path);
		log_error("This could be a security risk!\n");
		log_error("*********************************\n\n");
	}
	if (bitmask_is_set(path, 077077)) {
		log_error("*********************************\n");
		log_error("WARNING!!!\n");
		log_error("'%s' permissions are too permissive!\n", path);
		log_error("This could be a security risk!\n");
		log_error("*********************************\n\n");
	}
}

static void check_nvidia_devfiles(void)
{
	char *devs[] = {
		"/dev/nvidiactl",
		"/dev/nvidia-modeset",
		"/dev/nvidia-nvlink",
		"/dev/nvidia-nvswitchctl",
		"/dev/nvidia-uvm",
		"/dev/nvidia-uvm-tools",
		NULL
	};
	char *devs_fmt[] = {
		"/dev/nvidia%d",
		"/dev/nvidia-vgpu%d",
		"/dev/nvidia-nvswitch%d",
		"/dev/nvidiactl%d",
		NULL
	};
	char *nvidia_modprobe;
	const int max_gpus = 32, buflen = 1024;
	char **c, buf[buflen];
	int i;

	for (c = devs; *c != NULL; c++)
		check_right_permission(*c);
	for (c = devs_fmt; *c != NULL; c++) {
		for (i = 0; i < max_gpus; i++) {
			snprintf(buf, buflen, *c, i);
			check_right_permission(buf);
		}
	}
	nvidia_modprobe = which_nvidia_modprobe();
	if (nvidia_modprobe != NULL)
		check_right_permission(nvidia_modprobe);
	free(nvidia_modprobe);
}

#ifndef DISABLE_ENCRYPTION
static int init_cuda(void)
{
	size_t keylen = CHACHA_MASTER_KEY_WORDS * sizeof(uint32_t);
	void *key = alloca(keylen);
	log_notice("Generating key...\n");
	memshield_random(key, keylen);
	log_notice("Initializing cuda...\n");
	memshield_init(key);
	memset(key, 0, keylen);
	log_notice("Initializing cuda...done\n");
	return 0;
}
#endif

static void check_socket_parent_dir_perm(char *path)
{
	char *dir;
	struct stat sb;

	path = strdupa(path);
	dir = dirname(path);

	if (stat(dir, &sb)) {
		log_sys_error("Unable to stat \"%s\"\n", dir);
		return;
	}

	if (!S_ISDIR(sb.st_mode)) {
		log_error("\"%s\" is not a directory\n", dir);
		return;
	}

	if ((sb.st_mode & S_IWGRP) || (sb.st_mode & S_IWOTH)) {
		log_error("*********************************\n");
		log_error("WARNING!!!\n");
		log_error("'%s' should be writable only by root!\n", dir);
		log_error("This could be a security risk!\n");
		log_error("*********************************\n\n");
		return;
	}

	if ((sb.st_uid != 0) || (sb.st_gid != 0)) {
		log_error("*********************************\n");
		log_error("WARNING!!!\n");
		log_error("'%s' is not owned by root!\n", dir);
		log_error("This could be a security risk!\n");
		log_error("*********************************\n\n");
		return;
	}
}

int main(int argc, char *argv[])
{
	int sd;
	long cd;
	int ret;
	pthread_t t;
	pthread_attr_t attr;
	char *socket_path;
	struct sockaddr_un saddr;
	struct sockaddr_un caddr;
	socklen_t clen = sizeof(caddr);
	char *errptr;
	uint32_t ticket = 0;
	struct thread_data *td;

	if (argc != 2 && argc != 3) {
		log_error("Usage: %s <socket path> [<sliding window size>]\n",
			  argv[0]);
		return EXIT_FAILURE;
	}

	if (argc == 3) {
		errno = 0;
		ret = (int)strtol(argv[2], &errptr, 0);
		if (errno != 0 || *errptr != '\0' || ret <= 0) {
			log_error("Invalid sliding window size\n");
			return EXIT_FAILURE;
		}
		sliding_window_size = ret;
	}
	socket_path = argv[1];
	check_socket_parent_dir_perm(socket_path);
	check_nvidia_modprobe();
	check_nvidia_devfiles();

	log_debug("MemShield sliding window size: %u\n", sliding_window_size);
	log_profile("Profiling enabled\n");

	if (prctl(PR_SET_DUMPABLE, 0, 0, 0, 0) < 0) {
		log_sys_error("unable to disable process dumping\n");
		return EXIT_FAILURE;
	}

	if (!geteuid()) {
		if (mlockall(MCL_CURRENT | MCL_FUTURE) < 0) {
			log_sys_error("unable to mlockall()\n");
			return EXIT_FAILURE;
		}
	} else {
		log_warn("memory locking disabled (non-privileged user)\n");
	}

	PAGE_SIZE = sysconf(_SC_PAGESIZE);
	if (PAGE_SIZE == -1) {
		log_sys_error("sysconf() failed\n");
		return EXIT_FAILURE;
	}
	zero_page = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE,
			 MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if (zero_page == MAP_FAILED) {
		log_sys_error("zero mmap() failed\n");
		return EXIT_FAILURE;
	}
	memset(zero_page, 0, PAGE_SIZE);

#ifndef DISABLE_ENCRYPTION
	if (init_cuda()) {
		log_error("init_cuda failed!\n");
		return EXIT_FAILURE;
	}
#else
	log_warning("Encryption disabled\n");
#endif

	unlink(socket_path);

	sd = socket(AF_LOCAL, SOCK_STREAM, 0);
	if (sd == -1) {
		log_sys_error("error on socket()");
		return EXIT_FAILURE;
	}

	memset(&saddr, 0, SUN_LEN(&saddr));

	saddr.sun_family = AF_LOCAL;
	strncpy(saddr.sun_path, socket_path, sizeof(saddr.sun_path) - 1);

	ret = bind(sd, (struct sockaddr *)&saddr, SUN_LEN(&saddr));
	if (ret == -1) {
		log_sys_error("error on bind()");
		return EXIT_FAILURE;
	}
	if (chmod(socket_path, socket_mask) == -1) {
		log_sys_error("error on socket chmod()");
		return EXIT_FAILURE;
	}
	ret = listen(sd, LISTENQ);
	if (ret == -1) {
		log_sys_error("error on listen()");
		return EXIT_FAILURE;
	}

	if (pthread_attr_init(&attr)) {
		log_error("pthread_attr_init() failed\n");
		return EXIT_FAILURE;
	}

	if (pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)) {
		log_error("pthread_attr_setdetachstate() failed\n");
		return EXIT_FAILURE;
	}

	while (1) {
		cd = accept(sd, (struct sockaddr *)&caddr, &clen);
		if (cd == -1) {
			log_sys_error("error on accept()");
			break;
		}

		td = malloc(sizeof(*td));
		if (td == NULL) {
			log_error("malloc failed\n");
			close(cd);
			continue;
		}

		td->cd = cd;
		td->ticket = ticket++;
		if (pthread_create(&t, &attr, handle_fd, (void *)td)) {
			log_error("pthread_create() failed\n");
			free(td);
			close(cd);
		}
	}

	pthread_attr_destroy(&attr);
	close(sd);

	return EXIT_SUCCESS;
}
