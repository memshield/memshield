// SPDX-License-Identifier: GPL-2.0
#include <stdio.h>
#include <stdint.h>
#include <sched.h>

#include "log.h"
#include "memshield_chacha.h"

#define cuda_rotl32(v, n) ({					\
	uint64_t __tmp = ((((uint64_t)(v)) << 32U) | (v));	\
	__tmp >>= (32U - (n));					\
	(uint32_t) __tmp;					\
})

#define qround_gpu(a, b, c, d) do {					\
	(a) += (b); (d) ^= (a); (d) = __byte_perm((d), (d), 0x1032);	\
	(c) += (d); (b) ^= (c); (b) = cuda_rotl32((b), 12);		\
	(a) += (b); (d) ^= (a); (d) = __byte_perm((d), (d), 0x2103);	\
	(c) += (d); (b) ^= (c); (b) = cuda_rotl32((b), 7);		\
} while (0)

typedef uint32_t chacha_state_t[16] __attribute__((aligned(64)));

static __device__ __forceinline__
void chacha_init(chacha_state_t s, volatile uint32_t *key)
{
	s[0] = 0x61707865u;
	s[1] = 0x3320646eu;
	s[2] = 0x79622d32u;
	s[3] = 0x6b206574u;
	memcpy(s + 4, (const void *)key, CHACHA_MASTER_KEY_WORDS *
							sizeof(uint32_t));
}

static __device__ __forceinline__
void chacha_set_counter(chacha_state_t s, uint32_t cnt)
{
	s[12] = cnt; /* block counter */
}

static __device__ __forceinline__
void chacha_set_iv(chacha_state_t s, const void *__iv)
{
	const uint32_t *const iv = (uint32_t *) __iv;
	s[13] = iv[0];
	s[14] = iv[1];
	s[15] = iv[2];
}

static __device__ __forceinline__
void chacha_double_round(chacha_state_t s)
{
	qround_gpu(s[0], s[4], s[8], s[12]);
	qround_gpu(s[1], s[5], s[9], s[13]);
	qround_gpu(s[2], s[6], s[10], s[14]);
	qround_gpu(s[3], s[7], s[11], s[15]);
	qround_gpu(s[0], s[5], s[10], s[15]);
	qround_gpu(s[1], s[6], s[11], s[12]);
	qround_gpu(s[2], s[7], s[8], s[13]);
	qround_gpu(s[3], s[4], s[9], s[14]);
}

__device__ long2& operator^=(long2& a, const long2& b)
{
	a.x ^= b.x;
	a.y ^= b.y;

	return a;
}

__host__ __device__ long2 operator^(long2 a, long2 b)
{
	return make_long2(a.x ^ b.x, a.y ^ b.y);
}

__device__ int4 operator^(int4 a, int4 b)
{
	return make_int4(a.x ^ b.x, a.y ^ b.y, a.z ^ b.z, a.w ^ b.w);
}

__device__ int4& operator^=(int4& a, const int4& b)
{
	a.x ^= b.x;
	a.y ^= b.y;
	a.z ^= b.z;
	a.w ^= b.w;

	return a;
}

__device__ uint32_t dev_counter = 0;
__global__ void __gpu_chacha(struct page_queue *pq_array, uint32_t *key)
{
	int i;
	chacha_state_t is;
	const int tid = threadIdx.x;
	struct page_queue * const pq = pq_array + blockIdx.x;
	const struct page_data *const inv = pq->dev_slots->in;
	int4 *in;
	const uint32_t mask = pq->mask;
	volatile struct page_params *ppv = pq->params;
	uint32_t idx, h, t, flag = 0;

	union {
		chacha_state_t os;
		int4 v[4];
	};

	chacha_init(is, key);

	/* Thread 0 of each block atomically increments a counter in global
	 * memory. The last one erase the key from memory.
	 */
	if (threadIdx.x == 0)
		if (atomicAdd(&dev_counter, 1) == gridDim.x-1) {
			memset((void *) key, 0,
			       CHACHA_MASTER_KEY_WORDS * sizeof(uint32_t));
			__threadfence_system();
		}

	/* wait for thread 0 to do house-keeping stuff */
	__syncthreads();

	/* crypt cycle */
	while (1) {
		h = pq->head;
		t = pq->tail_dev;
		while (h < t) {
			idx = h & mask;
			in = (int4 *) inv[idx].data;

			chacha_set_counter(is, tid);

			do {
				flag = ppv[idx].flag;
			} while (!flag);


			chacha_set_iv(is, (void *)ppv[idx].iv);
			for (i = 0; i < 16; ++i)
				os[i] = is[i];

			for (i = 0; i < 10; ++i)
				chacha_double_round(os);

			for (i = 0; i < 16; ++i)
				os[i] += is[i];

			v[0] ^= in[tid + 0];
			v[1] ^= in[tid + 32];
			v[2] ^= in[tid + 64];
			v[3] ^= in[tid + 96];

			in[tid + 0] = v[0];
			in[tid + 32] = v[1];
			in[tid + 64] = v[2];
			in[tid + 96] = v[3];

			chacha_set_counter(is, tid+32);
			for (i = 0; i < 16; ++i)
				os[i] = is[i];

			for (i = 0; i < 10; ++i)
				chacha_double_round(os);

			for (i = 0; i < 16; ++i)
				os[i] += is[i];

			v[0] ^= in[tid + 128];
			v[1] ^= in[tid + 160];
			v[2] ^= in[tid + 192];
			v[3] ^= in[tid + 224];

			in[tid + 128] = v[0];
			in[tid + 160] = v[1];
			in[tid + 192] = v[2];
			in[tid + 224] = v[3];

			__threadfence_system();

			ppv[idx].flag = 0;
			++h;
		}
		pq->head = h;
	}
}

#define WARP_SIZE 32
void memshield_start(struct page_queue *pq_array, int num_workers, void *key)
{
	static cudaStream_t cstream, mstream;
	uint32_t host_counter = 0;

	if (cudaStreamCreate(&cstream) != cudaSuccess) {
		log_error("compute stream creation failed!\n");
		exit(EXIT_FAILURE);
	}

	if (cudaStreamCreate(&mstream) != cudaSuccess) {
		log_error("memory stream creation failed!\n");
		exit(EXIT_FAILURE);
	}

	__gpu_chacha<<<num_workers, WARP_SIZE, 0, cstream>>>(pq_array,
		(uint32_t *) key);

	do {
		int ret = cudaMemcpyFromSymbolAsync(&host_counter, dev_counter,
			sizeof(uint32_t), 0, cudaMemcpyDeviceToHost, mstream);
		if (ret != cudaSuccess) {
			log_error("unable to copy dev_counter to host.\n");
			exit(EXIT_FAILURE);
		}
	}
	while (host_counter != (uint32_t) num_workers);

	log_notice("%u workers ready\n", host_counter);

	memset(key, 0, CHACHA_MASTER_KEY_WORDS * sizeof(uint32_t));
}

int memshield_get_numworkers(void)
{
	int num_blocks;

	if (cudaOccupancyMaxActiveBlocksPerMultiprocessor(&num_blocks,
				__gpu_chacha, WARP_SIZE, 0) != cudaSuccess) {
		log_error("cudaOccupancyMaxActiveBlocksPerMultiprocessor "
								"failed\n");
		exit(EXIT_FAILURE);
	}

	return num_blocks;
}
