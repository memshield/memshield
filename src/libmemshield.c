// SPDX-License-Identifier: GPL-2.0
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <pthread.h>
#include <malloc.h>
#include <stdarg.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <signal.h>
#include <sched.h>

#include "log.h"
#include "userfaultfd_helper.h"

static char *memshield_socket;

static int memshield_sd;
static int memshield_uffd;
static int memshield_enabled = 0;
static pthread_t memshield_rpc_thread;
static void *memshield_shared_page;

static unsigned long PAGE_SIZE;
#define MEMSHIELD_DEFAULT_STACK_SIGNAL	SIGSTKFLT
#define MEMSHIELD_DEFAULT_STACK_SIZE	(1024 * PAGE_SIZE)

#define ALIGN_PAGE_DOWN(_x)	\
		(((unsigned long)(_x)) & ~(PAGE_SIZE-1UL))
#define ALIGN_PAGE_UP(_x)	\
		((((unsigned long)(_x)) + PAGE_SIZE-1UL) & (-PAGE_SIZE))

typedef void *(*orig_mmap_f_type)(void *addr, size_t length,
				  int prot, int flags, int fd, off_t offset);
static orig_mmap_f_type orig_mmap;

typedef void *(*orig_mmap64_f_type)(void *addr, size_t length,
				    int prot, int flags, int fd, off_t offset);
static orig_mmap64_f_type orig_mmap64;

typedef int (*orig_munmap_f_type)(void *addr, size_t length);
static orig_munmap_f_type orig_munmap;

typedef void *(*orig_mremap_f_type)(void *old_address, size_t old_size,
				    size_t new_size, int flags,
				    ... /*void *new_address */ );
static orig_mremap_f_type orig_mremap;

typedef void (*orig_free_f_type)(void *addr);
static orig_free_f_type orig_free;

typedef void *(*orig_malloc_f_type)(size_t size);
static orig_malloc_f_type orig_malloc;

typedef void *(*orig_calloc_f_type)(size_t nmemb, size_t size);
static orig_calloc_f_type orig_calloc = NULL;

typedef void *(*orig_realloc_f_type)(void *ptr, size_t size);
static orig_realloc_f_type orig_realloc;

typedef int (*orig_pthread_create_f_type)(pthread_t * thread,
					  const pthread_attr_t * attr,
					  void * (*start_routine)(void *),
					  void *arg);
static orig_pthread_create_f_type orig_pthread_create;

typedef pid_t(*orig_fork_f_type) (void);
static orig_fork_f_type orig_fork;

typedef int (*orig_execve_f_type)(const char *pathname,
				  char *const argv[], char *const envp[]);
static orig_execve_f_type orig_execve;

typedef int (*orig___libc_start_main_f_type)(int (*main)(int, char **, char **),
					     int argc, char **ubp_av,
					     void(*init)(void),
					     void(*fini)(void),
					     void(*rtld_fini)(void),
					     void(*stack_end));
static orig___libc_start_main_f_type orig___libc_start_main;

static void zero_mapped_memory(void *addr, size_t length)
{
	if (uffd_register_missing(memshield_uffd, addr, length) == -1)
		return;
	if (uffd_mprotect_wronly(addr, length) == -1)
		return;
	memset(addr, 0, length);
}

int munmap(void *addr, size_t length)
{
	if (!memshield_enabled)
		goto orig_munmap;

	log_info("stealth munmap %p (size: %lu)\n", addr, length);
	zero_mapped_memory(addr, length);

orig_munmap:
	return orig_munmap(addr, length);
}

static void register_to_uffd(void *addr, size_t size)
{
	void *alptr;
	size_t i, s;

	alptr = (void *)ALIGN_PAGE_DOWN(addr);
	s = ALIGN_PAGE_UP(size + (addr - alptr)) / PAGE_SIZE;
	for (i = 0; i < s; ++i)
		if (uffd_register_missing(memshield_uffd,
					  alptr + i * PAGE_SIZE,
					  PAGE_SIZE) == -1)
			_exit(EXIT_FAILURE);
}

void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset)
{
	void *ptr;

	if (flags & MAP_POPULATE)
		flags &= ~MAP_POPULATE;

	ptr = orig_mmap(addr, length, prot, flags, fd, offset);
	if (ptr == MAP_FAILED || !memshield_enabled)
		return ptr;
	if ((prot & PROT_EXEC) || (prot == PROT_NONE))
		return ptr;
	if (flags != (MAP_ANONYMOUS | MAP_PRIVATE))
		return ptr;
	register_to_uffd(ptr, ALIGN_PAGE_UP(length));
	log_info("stealth mmap (addr: %p - size: %lu)\n", ptr, length);

	return ptr;
}

void *mmap64(void *addr, size_t length, int prot, int flags, int fd,
	     off_t offset)
{
	log_info("stealth mmap64\n");
	return mmap(addr, length, prot, flags, fd, offset);
}

void *mremap(void *old_address, size_t old_size,
	     size_t new_size, int flags, ... /* void *new_address */ )
{
	void *ptr;
	void *new_address;
	va_list ap;
	int supported = 0;

	va_start(ap, flags);
	new_address = (flags & MREMAP_FIXED) ? va_arg(ap, void *) : NULL;
	va_end(ap);

	if (!memshield_enabled)
		goto orig_mremap;
	if (new_address != NULL) {
		log_warning("MREMAP_FIXED unimplemented\n");
		goto orig_mremap;
	}
	if (uffd_register_missing(memshield_uffd, old_address, old_size) == -1)
		goto orig_mremap;
	if (old_size > new_size) {
		ptr = (void *)ALIGN_PAGE_UP(old_address + new_size);
		log_info("remap: old: %p - start: %p - old_size: %lu "
			 "- new_size: %lu\n",
			 old_address + new_size, ptr, old_size, new_size);
		munmap(ptr, old_size - new_size);
	}
	supported = 1;
orig_mremap:
	ptr = orig_mremap(old_address, old_size, new_size, flags, new_address);
	if ((supported == 0) || (ptr == MAP_FAILED))
		return ptr;
	register_to_uffd(ptr, new_size);
	log_info("stealth mremap (old %p - new: %p) (old size: %lu - "
		 "new size: %lu)\n", old_address, ptr, old_size, new_size);
	return ptr;
}

void free(void *ptr)
{
	size_t size;

	if (!memshield_enabled || ptr == NULL)
		return orig_free(ptr);

	log_info("stealthing free %p\n", ptr);
	size = malloc_usable_size(ptr);
	memset(ptr, 0, size);
	log_info("memset done %p (size: %lu)\n", ptr, size);
	orig_free(ptr);
	log_info("stealth free %p (size: %lu)\n", ptr, size);
}

void *malloc(size_t size)
{
	void *ptr, *my_ptr;

	if (size == 0)
		return NULL;

	my_ptr = orig_mmap(NULL, ALIGN_PAGE_UP(size), PROT_READ | PROT_WRITE,
			   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if (my_ptr == MAP_FAILED) {
		log_error("malloc mmap failed %lu\n", size);
		return NULL;
	}

	ptr = orig_malloc(size);
	if (ptr == NULL || !memshield_enabled) {
		orig_munmap(my_ptr, ALIGN_PAGE_UP(size));
		return ptr;
	}
	memcpy(my_ptr, (void *)ALIGN_PAGE_DOWN(ptr), ALIGN_PAGE_UP(size));
	memset(ptr, 0, size);

	if (uffd_madvise_dontneed((void *)ALIGN_PAGE_DOWN(ptr),
				  ALIGN_PAGE_UP(size)) == -1) {
		log_sys_error("malloc madvise dontneed failed on addr %p\n",
			      ptr);
		_exit(EXIT_FAILURE);
	}

	log_info("stealth malloc %p (size: %lu)\n", ptr, size);

	register_to_uffd(ptr, size);
	memcpy((void *)ALIGN_PAGE_DOWN(ptr), my_ptr, ALIGN_PAGE_UP(size));
	orig_munmap(my_ptr, ALIGN_PAGE_UP(size));

	return ptr;
}

void *realloc(void *ptr, size_t size)
{
	void *new_ptr;
	size_t old_size;

	if (!memshield_enabled)
		return orig_realloc(ptr, size);
	if (ptr == NULL)
		return malloc(size);
	if (size == 0) {
		free(ptr);
		return NULL;
	}

	log_info("stealthing realloc %p (size: %lu)\n", ptr, size);
	old_size = malloc_usable_size(ptr);

	new_ptr = malloc(size);
	if (new_ptr == NULL)
		return NULL;

	memcpy(new_ptr, ptr, size > old_size ? old_size : size);
	free(ptr);
	log_info("stealth realloc %p (size: %lu) (new ptr: %p)\n", ptr,
		 size, new_ptr);
	return new_ptr;
}

void *calloc(size_t nmemb, size_t s)
{
	void *ptr, *my_ptr;
	size_t size = nmemb * s;

	if (nmemb == 0 || s == 0)
		return NULL;

	if (!memshield_enabled) {
		if (!orig_calloc)
			return NULL;
		return orig_calloc(nmemb, s);
	}

	log_debug("stealthing calloc\n");

	my_ptr = orig_mmap(NULL, ALIGN_PAGE_UP(size), PROT_READ | PROT_WRITE,
			   MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if (my_ptr == MAP_FAILED) {
		log_error("calloc mmap failed %lu\n", size);
		return NULL;
	}

	ptr = orig_calloc(nmemb, s);
	if (ptr == NULL) {
		orig_munmap(my_ptr, ALIGN_PAGE_UP(size));
		return NULL;
	}
	memcpy(my_ptr, (void *)ALIGN_PAGE_DOWN(ptr), ALIGN_PAGE_UP(size));
	memset(ptr, 0, size);

	if (uffd_madvise_dontneed((void *)ALIGN_PAGE_DOWN(ptr),
				  ALIGN_PAGE_UP(size)) == -1) {
		log_sys_error("calloc madvise dontneed failed on addr %p\n",
			      ptr);
		_exit(EXIT_FAILURE);
	}

	log_info("stealth calloc %p (size: %lu)\n", ptr, size);

	register_to_uffd(ptr, size);
	memcpy((void *)ALIGN_PAGE_DOWN(ptr), my_ptr, ALIGN_PAGE_UP(size));
	orig_munmap(my_ptr, ALIGN_PAGE_UP(size));
	return ptr;
}

pid_t fork(void)
{
	log_error("'fork' not supported by MemShield\n");
	_exit(EXIT_FAILURE);
}

int execve(const char *pathname, char *const argv[], char *const envp[])
{
	(void)pathname;
	(void)argv;
	(void)envp;
	log_error("'execve' not supported by MemShield\n");
	_exit(EXIT_FAILURE);
}

int pthread_create(pthread_t *thread, const pthread_attr_t *attr,
		   void *(*start_routine)(void *), void *arg)
{
	 (void)thread;
	(void)attr;
	(void)start_routine;
	(void)arg;
	log_error("'pthread_create' not supported by MemShield\n");
	_exit(EXIT_FAILURE);
}

static void disable_socket(int sd)
{
	if (close(sd) == -1)
		log_warning("error on socket close()\n");
}

static int enable_socket(char *server_path)
{
	int sd;
	struct sockaddr_un addr;

	sd = socket(AF_LOCAL, SOCK_STREAM, 0);
	if (sd == -1) {
		log_sys_error("error on socket()\n");
		return -1;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_LOCAL;
	strncpy(addr.sun_path, server_path, sizeof(addr.sun_path) - 1);

	if (connect(sd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
		log_sys_error("error on connect()\n");
		return -1;
	}

	return sd;
}

static int sendfd_on_socket(int sd, int fd)
{
	char buff;
	struct msghdr msg;
	struct iovec iov[1];
	union {
		struct cmsghdr cm;
		char control[CMSG_SPACE(sizeof(int))];
	} control_un;
	struct cmsghdr *cmptr;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof(control_un.control);
	cmptr = CMSG_FIRSTHDR(&msg);
	cmptr->cmsg_len = CMSG_LEN(sizeof(int));
	cmptr->cmsg_level = SOL_SOCKET;
	cmptr->cmsg_type = SCM_RIGHTS;
	memcpy(CMSG_DATA(cmptr), &fd, sizeof(int));
	msg.msg_name = NULL;
	msg.msg_namelen = 0;
	iov[0].iov_base = &buff;
	iov[0].iov_len = 1;
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;

	if ((sendmsg(sd, &msg, 0)) == -1) {
		log_sys_error("error on send fd sendmsg()\n");
		return -1;
	}

	return 0;
}

static int recvfd_from_socket(int sd)
{
	char buff;
	int recvfd;
	struct msghdr msg;
	struct iovec iov[1];

	union {
		struct cmsghdr cm;
		char control[CMSG_SPACE(sizeof(int))];
	} control_un;
	struct cmsghdr *cmsg;

	msg.msg_control = control_un.control;
	msg.msg_controllen = sizeof control_un.control;

	msg.msg_name = NULL;
	msg.msg_namelen = 0;

	iov[0].iov_base = &buff;
	iov[0].iov_len = sizeof buff;
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;

	if (recvmsg(sd, &msg, 0) == -1) {
		log_sys_error("error on recvmsg()");
		return -1;
	}

	cmsg = CMSG_FIRSTHDR(&msg);
	if (cmsg == NULL) {
		log_sys_error("error on handle_fd() #0\n");
		return -1;
	}

	if (cmsg->cmsg_len != CMSG_LEN(sizeof(int))) {
		log_sys_error("error on handle_fd() #1\n");
		return -1;
	}

	if (cmsg->cmsg_level != SOL_SOCKET) {
		log_sys_error("error on handle_fd() #2\n");
		return -1;
	}

	if (cmsg->cmsg_type != SCM_RIGHTS) {
		log_sys_error("error on handle_fd() #3\n");
		return -1;
	}

	memcpy(&recvfd, CMSG_DATA(cmsg), sizeof(int));

	return recvfd;
}

static void *memshield_rpc_job(void *args)
{
	(void)args;

	ssize_t ret;
	unsigned long addr;

	memshield_enabled = 1;
redo:
	ret = recv(memshield_sd, &addr, sizeof(addr), 0);
	if (ret != sizeof(addr)) {
		if (errno == EINTR)
			goto redo;

		log_sys_error("recv failed (%ld)\n", ret);
		_exit(EXIT_FAILURE);
	}

	log_debug("received addr %#lx\n", addr);

	memcpy(memshield_shared_page, (void *)addr, PAGE_SIZE);

	memset((void *)addr, 0, PAGE_SIZE);

	if (uffd_madvise_dontneed((void *)addr, PAGE_SIZE) == -1) {
		log_sys_error("madvise dontneed failed on address %#lx\n",
			      addr);
		_exit(EXIT_FAILURE);
	}

	ret = send(memshield_sd, &ret, sizeof(char), 0);
	if (ret != (long)sizeof(char)) {
		log_sys_error("page send failed (%ld)\n", ret);
		_exit(EXIT_FAILURE);
	}

	log_debug("sending addr %#lx\n", addr);

	goto redo;

	return NULL;
}

static int (*orig_main)(int, char **, char **);
static int orig_argc, orig_return = EXIT_FAILURE;
static volatile int memshield_sigstack_guard = 0;
static char **orig_argv, **orig_argp;

#define MEMSHIELD_SP __builtin_frame_address(0)
static void memshield_sigstack(int sig)
{
	if (signal(sig, SIG_DFL) == SIG_ERR) {
		log_sys_error("Unable to restore MemShield signal handler\n");
		_exit(EXIT_FAILURE);
	}
	log_info("start memshield_sigstack (stack: %p)\n", MEMSHIELD_SP);
	orig_return = orig_main(orig_argc, orig_argv, orig_argp);
	log_info("end memshield_sigstack (stack: %p)\n", MEMSHIELD_SP);
	__sync_synchronize();
	memshield_sigstack_guard = 1;
}

static int memshield_main(int argc, char **argv, char **argp)
{
	stack_t ss;
	struct sigaction act;
	void *memshield_stack;

	log_info("start memshield_main (stack: %p)\n", MEMSHIELD_SP);
	orig_argc = argc;
	orig_argv = argv;
	orig_argp = argp;
	memshield_stack = mmap(NULL, MEMSHIELD_DEFAULT_STACK_SIZE,
			       PROT_READ | PROT_WRITE,
			       MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
	if (memshield_stack == MAP_FAILED) {
		log_sys_error("Unable to alloc program stack\n");
		_exit(EXIT_FAILURE);
	}

	ss.ss_size = MEMSHIELD_DEFAULT_STACK_SIZE;
	ss.ss_flags = 0;
	ss.ss_sp = memshield_stack;
	if (sigaltstack(&ss, NULL) == -1) {
		log_sys_error("sigaltstack failed\n");
		_exit(EXIT_FAILURE);
	}
	memset(&act, 0, sizeof(act));
	act.sa_handler = memshield_sigstack;
	act.sa_flags = SA_ONSTACK;
	sigemptyset(&act.sa_mask);
	if (sigaction(MEMSHIELD_DEFAULT_STACK_SIGNAL, &act, NULL) == -1) {
		log_sys_error("stack sigaction failed\n");
		_exit(EXIT_FAILURE);
	}
	if (kill(getpid(), MEMSHIELD_DEFAULT_STACK_SIGNAL) == -1) {
		log_sys_error("stack kill failed\n");
		_exit(EXIT_FAILURE);
	}
	while (memshield_sigstack_guard == 0) {
		sched_yield();
		__sync_synchronize();
	}
	log_info("end memshield_main (stack: %p)\n", MEMSHIELD_SP);
	return orig_return;
}

int __libc_start_main(int (*main)(int, char **, char **), int argc,
		      char **ubp_av, void(*init)(void),
		      void(*fini)(void),
		      void(*rtld_fini)(void), void(*stack_end))
{
	if(getenv("MEMSHIELD_DISABLE_STACK") != NULL) {
		log_notice("MemShield stack protection disabled\n");
		return orig___libc_start_main(main, argc, ubp_av, init, fini,
					      rtld_fini, stack_end);
	}
	orig_main = main;
	return orig___libc_start_main(memshield_main, argc, ubp_av, init, fini,
				      rtld_fini, stack_end);
}

void __attribute__((constructor)) libmemshield_init(void)
{
	int ret, memfd;
	pthread_attr_t rpc_thread_attr;
	long ps;

	if (mallopt(M_MMAP_THRESHOLD, 0) != 1) {
		log_error("mallopt MMAP_THRESHOLD failed\n");
		_exit(EXIT_FAILURE);
	}
	malloc_trim(0);

	ps = sysconf(_SC_PAGESIZE);
	if (ps == -1)
		_exit(EXIT_FAILURE);

	PAGE_SIZE = ps;
	memshield_socket = getenv("MEMSHIELD_SOCKET");
	if (memshield_socket == NULL) {
		log_error("Unable to read MemShield socket path\n");
		log_error("Please set 'MEMSHIELD_SOCKET' env var\n");
		_exit(EXIT_FAILURE);
	}

	log_info("Libmemshield_init started (%s)\n", memshield_socket);

	orig_mmap = (orig_mmap_f_type) dlsym(RTLD_NEXT, "mmap");
	if (!orig_mmap)
		_exit(EXIT_FAILURE);

	orig_mmap64 = (orig_mmap64_f_type) dlsym(RTLD_NEXT, "mmap64");
	if (!orig_mmap64)
		_exit(EXIT_FAILURE);

	orig_munmap = (orig_munmap_f_type) dlsym(RTLD_NEXT, "munmap");
	if (!orig_munmap)
		_exit(EXIT_FAILURE);

	orig_malloc = (orig_malloc_f_type) dlsym(RTLD_NEXT, "malloc");
	if (!orig_malloc)
		_exit(EXIT_FAILURE);

	orig_calloc = (orig_calloc_f_type) dlsym(RTLD_NEXT, "calloc");
	if (!orig_calloc)
		_exit(EXIT_FAILURE);

	orig_free = (orig_free_f_type) dlsym(RTLD_NEXT, "free");
	if (!orig_free)
		_exit(EXIT_FAILURE);

	orig_mremap = (orig_mremap_f_type) dlsym(RTLD_NEXT, "mremap");
	if (!orig_mremap)
		_exit(EXIT_FAILURE);

	orig_realloc = (orig_realloc_f_type) dlsym(RTLD_NEXT, "realloc");
	if (!orig_realloc)
		_exit(EXIT_FAILURE);

	orig_pthread_create = (orig_pthread_create_f_type) dlsym(RTLD_NEXT,
								 "pthread_create");
	if (!orig_pthread_create)
		_exit(EXIT_FAILURE);

	orig_fork = (orig_fork_f_type) dlsym(RTLD_NEXT, "fork");
	if (!orig_fork)
		_exit(EXIT_FAILURE);

	orig_execve = (orig_execve_f_type) dlsym(RTLD_NEXT, "execve");
	if (!orig_execve)
		_exit(EXIT_FAILURE);

	orig___libc_start_main =
	    (orig___libc_start_main_f_type) dlsym(RTLD_NEXT,
						  "__libc_start_main");
	if (!orig___libc_start_main)
		_exit(EXIT_FAILURE);

	memshield_uffd = open_userfault_fd();
	if (memshield_uffd == -1)
		_exit(EXIT_FAILURE);

	memshield_sd = enable_socket(memshield_socket);
	if (memshield_sd == -1)
		_exit(EXIT_FAILURE);

	ret = sendfd_on_socket(memshield_sd, memshield_uffd);
	if (ret == -1)
		_exit(EXIT_FAILURE);

	memfd = recvfd_from_socket(memshield_sd);
	if (memfd == -1)
		_exit(EXIT_FAILURE);
	memshield_shared_page = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE,
				     MAP_SHARED | MAP_POPULATE, memfd, 0);
	if (memshield_shared_page == MAP_FAILED)
		_exit(EXIT_FAILURE);
	if (close(memfd) == -1)
		_exit(EXIT_FAILURE);

	ret = send(memshield_sd, &ret, sizeof(char), 0);
	if (ret != (long)sizeof(char))
		_exit(EXIT_FAILURE);

	ret = pthread_attr_init(&rpc_thread_attr);
	if (ret != 0) {
		log_sys_error("pthread attr init()\n");
		_exit(EXIT_FAILURE);
	}

	ret = pthread_attr_setdetachstate(&rpc_thread_attr,
					  PTHREAD_CREATE_DETACHED);
	if (ret != 0) {
		log_sys_error("pthread set detach()\n");
		_exit(EXIT_FAILURE);
	}

	ret = orig_pthread_create(&memshield_rpc_thread, &rpc_thread_attr,
				  memshield_rpc_job, NULL);
	if (ret != 0) {
		log_sys_error("pthread create()\n");
		_exit(EXIT_FAILURE);
	}
	while (memshield_enabled == 0) {
		sched_yield();
		__sync_synchronize();
	}
	log_info("libmemshield started\n");
}

void __attribute__((destructor)) libmemshield_exit(void)
{
	memshield_enabled = 0;
	log_debug("libmemshield ending\n");
	if (pthread_cancel(memshield_rpc_thread))
		log_warning("pthread cancel failed\n");

	munmap(memshield_shared_page, PAGE_SIZE);
	disable_socket(memshield_sd);
	close_userfault_fd(memshield_uffd);
	log_debug("libmemshield end\n");
}
